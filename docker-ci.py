import argparse
import subprocess

def main():
    command_parser = argparse.ArgumentParser(add_help=False)
    command_parser.add_argument('action', choices=["build", "publish"])
    command_args, _ = command_parser.parse_known_args()

    parser = argparse.ArgumentParser(prog='docker-ci', parents=[command_parser], description='DevOps P3 CI toolkit')
    
    parser.add_argument('--tag', help='The Docker image tag')
        
    if command_args.action == 'publish':
        parser.add_argument('project')
        parser.add_argument('--username', help='The Gitlab registry username')
        parser.add_argument('--password', help='The Gitlab registry password')
        parser.add_argument('--registry', help='The Gitlab registry', default="registry.gitlab.com")

    args = parser.parse_args()
    fn = globals()[command_args.action]
    fn(args)


def build(args):
    command = ["docker", "build"]
    if (args.tag):
        command += ["-t", args.tag]
    command += ["."]
    subprocess.call(command)

def publish(args):
    if args.username and args.password:
        subprocess.call(["docker", "login", args.registry, "--username", args.username, "--password", args.password])

    subprocess.call(["docker", "push", args.project])


if __name__ == "__main__":
    main()

